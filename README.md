**This template is based on https://github.com/greenelab/lab-website-template**

To test and develop this template locally:

    bundle install
    bundle exec jekyll serve

# Template Pages for a Group Page
Collection of group website pages hosted in GitHub or GitLab:

## Templates
- Lab Website Template, [repository](https://github.com/greenelab/lab-website-template), [website](https://greenelab.github.io/lab-website-template/team/) 
- Research Group Template, [repository](https://github.com/wowchemy/starter-hugo-research-group), [website](https://research-group.netlify.app/)
- Research Lab, [repository](https://github.com/ericdaat/research-lab-website), [website](https://research-lab.edaoud.com/)
- Academic pages, [repository](https://github.com/pankajkgupta/pankajkgupta.github.io), [website](https://academicpages.github.io/) 
- [Wowchemy templates collection](https://wowchemy.com/creators/)

## Minimal real examples
- RT2 Lab, [repository](https://github.com/rt2lab/rt2lab.github.io), [website](https://rt2lab.github.io/) 

## Group / Project websites, multiple sections
- Internet Security Research Group (ISRG), [repository](https://github.com/abetterinternet/website), [website](https://www.abetterinternet.org/) 
- Computer Science & Engineering Association, NIT Calicut, [repository](https://github.com/csea-nitc/CSEA-Assoc_Website), [website](http://assoc.cse.nitc.ac.in/) 
- Structure Processing Property Research, [repository](https://github.com/prasadheeramani/spp.github.io), [website](https://prasadheeramani.github.io/spp.github.io/) 
- Design Research Collective, [repository](https://github.com/cmudrc/cmudrc.github.io), [website](https://cmudrc.github.io/) 
- THUIAR, [repository](https://github.com/MFYDev/THUIARWeb), [website](https://thuiar.github.io/) 
- NBClab, [repository](https://github.com/NBCLab/NBCLab.github.io), [website](https://nbclab.github.io/) 
- AMPP Lab, [repository](https://github.com/AMPP-Lab/lab-website-home), [website](https://ampp-lab.github.io/lab-website-home/) 

## Animations / modern look
- Citizen Science Project, [repository](https://gitlab.com/YannickMassard/simple-citizen-science), [website](https://yannickmassard.gitlab.io/simple-citizen-science/) 

## CV websites
- About Me, [repository](https://gitlab.com/brandonp2412/about-me), [website](https://brandonp2412.gitlab.io/about-me/)
- Dominik Adamek, [repository](https://gitlab.com/dominik716/adamekdominik_iosstyle), [website](https://dominik716.gitlab.io/adamekdominik_iosstyle/)  